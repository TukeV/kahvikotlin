# KahviKotlin

KahviKotlin is an Android app, which acts as a remote controller for my [coffee maker](https://bitbucket.org/TukeV/coffee-server/). The app is made with Kotlin.