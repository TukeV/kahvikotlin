package com.example.tuke.kahvikotlin

import android.annotation.SuppressLint
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.Switch
import android.widget.TextView
import java.text.SimpleDateFormat

class HomeFragment : Fragment(), CoffeeClient.CoffeeListener {

    private lateinit var serverStatus: Switch
    private lateinit var powerStatus: Switch
    private lateinit var powerOnButton: Button
    private lateinit var powerOffButton: Button
    private lateinit var timerText: TextView
    private lateinit var timeoutText: TextView

    var coffeeClient: CoffeeClient? = null

    @SuppressLint("SimpleDateFormat")
    private val dateTimeFormat = SimpleDateFormat("dd.MM.yyyy HH:mm")

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        serverStatus = view.findViewById(R.id.onoff_switch)
        powerStatus = view.findViewById(R.id.power_status)
        powerOffButton = view.findViewById(R.id.off_button)
        powerOnButton = view.findViewById(R.id.on_button)
        timerText = view.findViewById(R.id.timer_text)
        timeoutText = view.findViewById(R.id.timeout_text)

        powerOffButton.setOnClickListener{
            coffeeClient!!.sendPowerRequest(false)
        }
        powerOnButton.setOnClickListener{
            coffeeClient!!.sendPowerRequest(true)
        }
    }

    override fun onCoffeeStatusChanged(newStatus: CoffeeClient.ServerStatus) {
        serverStatus.setText(R.string.online)
        serverStatus.isChecked = true
        powerStatus.isChecked = newStatus.power
        val powerText = if (newStatus.power) R.string.power_on else R.string.power_off
        powerStatus.setText(powerText)
        if(newStatus.timer == null){
            timerText.text = resources.getString(R.string.unknown_datetime)
        }else{
            timerText.text = dateTimeFormat.format(newStatus.timer)
        }
        if(newStatus.timeout == null){
            timeoutText.text = resources.getString(R.string.unknown_datetime)
        }else{
            timeoutText.text = dateTimeFormat.format(newStatus.timeout)
        }
    }

    override fun onUpdateFailure(reason: String) {
        serverStatus.isChecked = false
        serverStatus.setText(R.string.online)
        powerStatus.setText(R.string.power_off)
        powerStatus.isChecked = false
        timerText.text = resources.getString(R.string.unknown_datetime)
        timeoutText.text = resources.getString(R.string.unknown_datetime)

    }

}
