package com.example.tuke.kahvikotlin

import android.content.Context
import android.os.Bundle
import android.support.design.widget.BottomNavigationView
import android.support.v4.view.ViewPager
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import android.support.v7.app.AppCompatActivity


class MainActivity : AppCompatActivity(), CoffeeClient.CoffeeListener{

    /*TAGS*/
    companion object {
        const val TAG = "MainActivity"
        const val APP_SETTINGS = "CoffeeRemoteSettings"

        // Shared Preferences
        const val USERNAME = "USERNAME"
        const val PASSWORD = "PASSWORD"
        const val ADDRESS = "ADDRESS"
    }

    // ViewPager
    private lateinit var viewPager: ViewPager

    // Fragments
    private lateinit var homeFragment: HomeFragment
    private lateinit var timerFragment: TimerFragment
    private lateinit var serverFragment: ServerFragment

    // CoffeeClient
    lateinit var coffeeClient: CoffeeClient

    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {
            R.id.navigation_home -> {
                viewPager.currentItem = 0
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_timer -> {
                viewPager.currentItem = 1
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_server -> {
                viewPager.currentItem = 2
                return@OnNavigationItemSelectedListener true
            }
        }
        false
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val navigation: BottomNavigationView = findViewById(R.id.navigation)
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
        viewPager = findViewById(R.id.viewpager)
        setupViewPager()
        setupClient()
    }

    private fun setupViewPager() {
        val pagerAdapter = ViewPagerAdapter(supportFragmentManager)
        homeFragment = HomeFragment()
        timerFragment = TimerFragment()
        serverFragment = ServerFragment()
        pagerAdapter.addFragment(homeFragment)
        pagerAdapter.addFragment(timerFragment)
        pagerAdapter.addFragment(serverFragment)
        viewPager.adapter = pagerAdapter
    }

    private fun setupClient() {
        val settings = getSharedPreferences(APP_SETTINGS, Context.MODE_PRIVATE)
        val address = settings.getString(ADDRESS, "http://0.0.0.0:5000")
        val username = settings.getString(USERNAME, "Username")
        val password = settings.getString(PASSWORD, "password")
        Log.d(TAG, String.format("%s:%s@%s", username, password, address))
        coffeeClient = CoffeeClient(username= username, password = password, address = address)
        coffeeClient.addListener(homeFragment)
        coffeeClient.addListener(timerFragment)
        coffeeClient.addListener(serverFragment)
        coffeeClient.addListener(this)
        homeFragment.coffeeClient = this.coffeeClient
        timerFragment.coffeeClient = this.coffeeClient
        serverFragment.coffeeClient = this.coffeeClient
    }

    private fun writePreferences() {
        val settings = getSharedPreferences(APP_SETTINGS, Context.MODE_PRIVATE)
        val editor = settings.edit()
        editor.putString(ADDRESS, coffeeClient.address)
        editor.putString(USERNAME, coffeeClient.username)
        editor.putString(PASSWORD, coffeeClient.password)
        editor.apply()
        editor.commit()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.update_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        Log.d(TAG, if (item.itemId == R.id.update_button) "Updatebutton" else "Not")
        if (item.itemId == R.id.update_button) {
            coffeeClient.sendStatusRequest()
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onCoffeeStatusChanged(newStatus: CoffeeClient.ServerStatus) {
        if(newStatus.power) {
            timeoutNotification()
        }else if(newStatus.timer != null){
            timerNotification()
        }
    }

    private fun timerNotification() {}

    private fun timeoutNotification() {}

    override fun onUpdateFailure(reason: String) {
        Toast.makeText(applicationContext, reason, Toast.LENGTH_LONG).show()
    }
}
