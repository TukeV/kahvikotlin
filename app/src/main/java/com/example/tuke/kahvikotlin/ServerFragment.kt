package com.example.tuke.kahvikotlin

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText

class ServerFragment : Fragment(), CoffeeClient.CoffeeListener {
    lateinit var inputUsername: EditText
    lateinit var inputPassword: EditText
    lateinit var inputAddress: EditText

    var coffeeClient: CoffeeClient? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_server, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        inputUsername = view.findViewById(R.id.input_username)
        inputPassword = view.findViewById(R.id.input_password)
        inputAddress = view.findViewById(R.id.input_address)
        setupEditListeners()
        activity?.let {
            val settings = it.getSharedPreferences(MainActivity.APP_SETTINGS, Context.MODE_PRIVATE)
            val username = settings.getString(MainActivity.USERNAME, getString(R.string.username))
            val password = settings.getString(MainActivity.PASSWORD, getString(R.string.hidden_placeholder))
            val address = settings.getString(MainActivity.ADDRESS, getString(R.string.placeholder_address))
            setFields(username=username, password = password, address = address)
            coffeeClient?.updateAddress(address = address)
        } ?: run{
            setFields(username="", password="", address="")
        }
    }

    private fun setFields(username: String?=null, password: String?=null, address: String?=null){
        if(username != null) inputUsername.setText(username)
        if(password != null) inputPassword.setText(password)
        if(address != null) inputAddress.setText(address)
    }

    override fun onCoffeeStatusChanged(newStatus: CoffeeClient.ServerStatus) {}

    override fun onUpdateFailure(reason: String) {}

    private fun setupEditListeners(){
        inputUsername.addTextChangedListener(object: TextWatcher {
            override fun afterTextChanged(p0: Editable?) {}
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if(coffeeClient != null) {
                    coffeeClient!!.updateAuth(username = s.toString())
                    activity?.let {
                        val settings = it.getSharedPreferences(MainActivity.APP_SETTINGS, Context.MODE_PRIVATE)
                        val editor = settings.edit()
                        editor.putString(MainActivity.USERNAME, coffeeClient!!.username)
                        editor.apply()
                        editor.commit()
                        Log.d("Username changed", coffeeClient!!.username)
                    }
                }
            }
        })
        inputPassword.addTextChangedListener(object: TextWatcher {
            override fun afterTextChanged(p0: Editable?) {}
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if(coffeeClient != null) {
                    coffeeClient!!.updateAuth(password = s.toString())
                    activity?.let{
                        val settings = it.getSharedPreferences(MainActivity.APP_SETTINGS, Context.MODE_PRIVATE)
                        val editor = settings.edit()
                        editor.putString(MainActivity.PASSWORD, coffeeClient!!.password)
                        editor.apply()
                        editor.commit()
                        Log.d("Password changed", coffeeClient!!.password)
                    }
                }
            }
        })

        inputAddress.addTextChangedListener(object: TextWatcher {
            override fun afterTextChanged(p0: Editable?) {}
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if(coffeeClient != null) {
                    coffeeClient!!.updateAddress(s.toString())
                    activity?.let {
                        val settings = it.getSharedPreferences(MainActivity.APP_SETTINGS, Context.MODE_PRIVATE)
                        val editor = settings.edit()
                        editor.putString(MainActivity.ADDRESS, coffeeClient!!.address)
                        editor.apply()
                        editor.commit()
                        Log.d("Address changed", coffeeClient!!.address)
                    }
                }
            }
        })
    }
}

