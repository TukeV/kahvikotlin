package com.example.tuke.kahvikotlin

import android.util.Base64
import android.util.Log
import com.github.kittinunf.fuel.Fuel
import com.github.kittinunf.fuel.android.core.Json
import com.github.kittinunf.fuel.android.extension.responseJson
import com.github.kittinunf.fuel.core.FuelError
import com.github.kittinunf.fuel.core.FuelManager
import com.github.kittinunf.fuel.core.Response
import com.github.kittinunf.result.Result
import org.json.JSONObject
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

/**
 * Created by Tuke on 1.10.2017.
 */

class CoffeeClient (var username: String, var password: String, var address: String){

    interface CoffeeListener {
        fun onCoffeeStatusChanged(newStatus: ServerStatus)
        fun onUpdateFailure(reason: String)
    }

    data class ServerStatus(
            var power: Boolean,
            var timer: Date?,
            var timeout: Date?
    )

    init {
        updateAuth(username, password)
        updateAddress(address)
    }

    companion object {
        val datetimeFormat = SimpleDateFormat("yyyy-MM-dd HH:mm")
    }

    private var basicAuth: String = ""


    private var URL_STATUS = ""
    private var URL_POWER = ""
    private var URL_TIMER = ""
    private var URL_TIMEOUT = ""


    var eventListeners: ArrayList<CoffeeListener> = ArrayList()
    fun addListener(listener: CoffeeListener){ eventListeners.add(listener)}

    fun updateAuth(username: String=this.username, password: String=this.password) {
        this.username = username
        this.password = password
        val plainAuth = String.format("%s:%s", username, password)
        this.basicAuth = String.format("Basic %s", Base64.encodeToString(plainAuth.toByteArray(), Base64.DEFAULT))
        Log.d("username", username)
        Log.d("password", password)
        Log.d("BasicAuth", basicAuth)
    }

    fun updateAddress(address: String){
        this.address = address
        URL_STATUS = String.format("%s/status", address)
        URL_POWER = String.format("%s/power", address)
        URL_TIMER = String.format("%s/timer", address)
        URL_TIMEOUT = String.format("%s/timeout", address)
        FuelManager.instance.basePath = address
        Log.d("URL_STATUS", URL_STATUS)
        Log.d("URL_POWER", URL_POWER)
        Log.d("URL_TIMER", URL_TIMER)
        Log.d("URL_TIMEOUT", URL_TIMEOUT)
    }

    fun sendStatusRequest(){
        Fuel.get("/status")
                .header("Content-type" to "application/json")
                .authenticate(username, password)
                .timeout(1000).timeoutRead(5000)
                .responseJson { _, response, result ->
                    handleResult(response, result)
                }
    }

    fun sendPowerRequest(on: Boolean) {
        val body = """{"power":"%s"}""".format(on.toString())
        Log.d("PowerRequest", body)
        Fuel.put("/power")
                .authenticate(username, password)
                .header("Content-type" to "application/json")
                .timeout(1000).timeoutRead(5000)
                .body(body)
                .responseJson { _, response, result ->
                    handleResult(response, result)
                }
    }


    fun sendTimeoutRequest(timeout: Date){
        val datetime = datetimeFormat.format(timeout)
        val body = """{"datetime":"%s"}""".format(datetime)
        Log.d("TiemoutRequest", body)
        Fuel.put("/timeout")
                .authenticate(username, password)
                .header("Content-type" to "application/json")
                .timeout(1000).timeoutRead(5000)
                .body(body)
                .responseJson { _, response, result ->
                    handleResult(response, result)
                }
    }


    fun sendTimerRequest(timer: Date){
        val datetime = datetimeFormat.format(timer)
        val body = """{"datetime":"%s"}""".format(datetime)
        Log.d("TimerRequest", body)
        Fuel.put("/timer")
                .authenticate(username, password)
                .header("Content-type" to "application/json")
                .timeout(1000).timeoutRead(5000)
                .body(body)
                .responseJson { _, response, result ->
                    handleResult(response, result)
                }
    }

    private fun handleResult(response: Response, result: Result<Json, FuelError>){
        Log.d("Response Handler", "%d - %s".format(response.statusCode, response.responseMessage))
        when(response.statusCode){
            -1 ->{
                Log.e("Failure", result.toString())
                updateFailed("Cannot connect to the server")
            }
            in 200..299 ->{
                val data = result.get()
                Log.i("Success", data.content)
                updateStatus(data.obj())
            }
            in 400..499 ->{
                Log.e("Failure", response.toString())
                val errorBody = String(response.data)
                updateFailed(errorBody)
            }
            in 500..599 ->{
                Log.e("Failure", response.toString())
                val errorBody = String(response.data)
                updateFailed(errorBody)
            }
        }
    }


    private fun updateStatus(response: JSONObject) {
        fun parseDateTime(datetime: String): Date?{
            if(datetime == "") return null
            return datetimeFormat.parse(datetime)
        }
        val power = response.getBoolean("power")
        val timer = parseDateTime(response.getString("timer"))
        val timeout = parseDateTime(response.getString("timeout"))
        val serverStatus = ServerStatus(power = power, timer = timer, timeout = timeout)
        for(listener in eventListeners){
            listener.onCoffeeStatusChanged(serverStatus)
        }
    }

    private fun updateFailed(reason: String){
        for(listener in eventListeners){
            listener.onUpdateFailure(reason)
        }
    }

}