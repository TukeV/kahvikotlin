package com.example.tuke.kahvikotlin


import android.annotation.SuppressLint
import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.DatePicker
import android.widget.EditText
import android.widget.TimePicker
import java.text.SimpleDateFormat
import java.util.*


class TimerFragment : Fragment(), CoffeeClient.CoffeeListener{

    @SuppressLint("SimpleDateFormat")
    private val dateTimeFormat = SimpleDateFormat("dd.MM.yyyy HH:mm")
    @SuppressLint("SimpleDateFormat")
    private val dateFormater = SimpleDateFormat("dd.MM.yyyy")
    @SuppressLint("SimpleDateFormat")
    private val timeFormater = SimpleDateFormat("HH:mm")

    // Set Timer
    lateinit var timerTimeInput :EditText
    lateinit var timerDateInput :EditText
    lateinit var timerButton :Button

    // Set Timeout
    lateinit var timeoutTimeInput :EditText
    lateinit var timeoutDateInput :EditText
    lateinit var timeoutButton :Button

    var coffeeClient: CoffeeClient? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_timer, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        // Get Views
        timerTimeInput = view.findViewById(R.id.input_timer_time)
        timerDateInput = view.findViewById(R.id.input_timer_date)
        timerButton = view.findViewById(R.id.timer_button)
        timeoutTimeInput = view.findViewById(R.id.input_timeout_time)
        timeoutDateInput = view.findViewById(R.id.input_timeout_date)
        timeoutButton = view.findViewById(R.id.timeout_button)
        // Set OnClickListeners for editTexts
        timerTimeInput.setOnClickListener{setTime(timerTimeInput)}
        timerDateInput.setOnClickListener{setDate(timerDateInput, timerTimeInput)}
        timeoutDateInput.setOnClickListener{setDate(timeoutDateInput, timeoutTimeInput)}
        timeoutTimeInput.setOnClickListener{setTime(timeoutTimeInput)}
        // Set onClickListeners for buttons
        timerButton.setOnClickListener{
            val datetimeString = String.format("%s %s", timerDateInput.text, timerTimeInput.text)
            val date = dateTimeFormat.parse(datetimeString)
            coffeeClient!!.sendTimerRequest(date)
        }
        timeoutButton.setOnClickListener{
            val datetimeString = String.format("%s %s", timeoutDateInput.text, timeoutTimeInput.text)
            val date = dateTimeFormat.parse(datetimeString)
            coffeeClient!!.sendTimeoutRequest(date)
        }
    }

    override fun onCoffeeStatusChanged(newStatus: CoffeeClient.ServerStatus) {
        if(newStatus.timer == null){
            timerTimeInput.setText(resources.getString(R.string.unknown_time))
            timerDateInput.setText(resources.getString(R.string.unknown_date))
        }else{
            timerTimeInput.setText(timeFormater.format(newStatus.timer))
            timerDateInput.setText(dateFormater.format(newStatus.timer))
        }
        if(newStatus.timeout == null){
            timeoutTimeInput.setText(resources.getString(R.string.unknown_time))
            timeoutDateInput.setText(resources.getString(R.string.unknown_date))
        }else{
            timeoutTimeInput.setText(timeFormater.format(newStatus.timeout))
            timeoutDateInput.setText(dateFormater.format(newStatus.timeout))
        }
    }

    override fun onUpdateFailure(reason: String) {}

    fun setDate(dateField: EditText, timeField: EditText){
        val c = Calendar.getInstance()
        val year = c.get(Calendar.YEAR)
        val month = c.get(Calendar.MONTH)
        val day = c.get(Calendar.DATE)
        val datePickerDialog = DatePickerDialog(this.context, DatePickerDialog.OnDateSetListener { _, selectedYear, selectedMonth, selectedDay ->
            val selectedMonth = selectedMonth + 1
            val strYear = selectedYear.toString()
            val strMonth = if (selectedMonth < 10) String.format("0%d", selectedMonth) else selectedMonth.toString()
            val strDay = if (selectedDay < 10) String.format("0%d", selectedDay) else selectedDay.toString()
            dateField.setText(String.format("%s.%s.%s", strDay, strMonth, strYear))
            timeField.callOnClick()
        }, year, month, day)
        datePickerDialog.setTitle("Select Day")
        datePickerDialog.show()
    }

    fun setTime(field: EditText){
        val c = Calendar.getInstance()
        val hour = c.get(Calendar.HOUR_OF_DAY)
        val minute = c.get(Calendar.MINUTE)
        val mTimePicker: TimePickerDialog
        mTimePicker = TimePickerDialog(this.context, TimePickerDialog.OnTimeSetListener { _, selectedHour, selectedMinute ->
            val strHour = if (selectedHour < 10) String.format("0%d", selectedHour) else selectedHour.toString()
            val strMin = if (selectedMinute < 10) String.format("0%d", selectedMinute) else selectedMinute.toString()
            field.setText(String.format("%s:%s", strHour, strMin))
        }, hour, minute, true)
        mTimePicker.setTitle("Select Time")
        mTimePicker.show()
    }
}
